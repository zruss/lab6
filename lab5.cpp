/*********
 * Zachary Russ
 * zruss
 * Lab B108
 * Section 003
 * Nushrat
*********/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

//Card struct definition
typedef struct Card {
  Suit suit;
  int value;
} Card;

//Function prototypes
string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[])
{
  //Seed random number generator
  srand(unsigned (time(0)));

  //Create deck of cards
  Card deck[52];
  for(int i = 0; i < 52; i++)
  {
    deck[i].value = i%13+2;
    if(i < 13)
    {
      deck[i].suit = SPADES;
    }
    else if(i > 12 && i < 26)
    {
      deck[i].suit = HEARTS;
    }
    else if(i > 25 && i < 39)
    {
      deck[i].suit = DIAMONDS;
    }
    else
    {
      deck[i].suit = CLUBS;
    }
  }

  //Shuffle entire deck
  random_shuffle(&deck[0], &deck[51], myrandom);

  //Create hand of first five cards from shuffled deck
  Card hand[5] = {deck[0], deck[1], deck[2], deck[3], deck[4]};

  //Sort the cards
  sort(&hand[0], &hand[5], suit_order);

  //Print out the cards
  for(int i = 0; i < 5; i++)
  {
    cout << setw(5) << left << get_card_name(hand[i]) << " of " << get_suit_code(hand[i]) <<  endl;
  }

  //Tell the computer that everything is okay.
  return 0;

}

//Sorting function parameter function
bool suit_order(const Card& lhs, const Card& rhs)
{
  //Sort by suits first
  if(lhs.suit != rhs.suit)
  {
    if(lhs.suit < rhs.suit)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  else
  {
    //Sort by value second
    if(lhs.value < rhs.value)
    {
      return true;
    }
    else
    {
      return false;
    }
    
  }
}

//Get unicode character for suit string
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

//Get card name string from value
string get_card_name(Card& c)
{
  if(c.value == 11)
  {
    return "JACK";
  }
  else if(c.value == 12)
  {
    return "QUEEN";
  }
  else if(c.value == 13)
  {
    return "KING";
  }
  else if(c.value == 14)
  {
    return "ACE";
  }
  else
  {
    return to_string(c.value);
  }
}